module.exports = {
  cookies: {
    accept: 'Aceptar',
    message: 'Éste sitio web usa cookies, si permanece aquí acepta su uso.',
  },
  head: {
    title: 'sabiduria ancetral',
    meta: {
      author: 'Maica',
      url: 'https://curapachamama.com',
      keywords: 'ayahuasca, kambo, yopo, rape, amazonas, espiritu, amazonicas, espiritual, pachamama',
      home: {
        description: 'Centro holistico de medicinas amazónicas y del mundo, que se han usado desde hace milenios para la cura de dolencias fisicas, psíquicas y espirituales. Dietas amazónicas y chamánicas con plantas maestras, Ceremonias de Ayahuasca, Kambó, Yopo, Círculos de Rapé, y más. Viajes a la selva peruana.',
        title: 'sabiduria ancetral',
      },
    },
  },
  app: {
    home: {
      'phone-me': 'Contactame',
      presentation: {
        title: 'Sabiduria Ancestral',
        description: '"El saber y el poder de la Madre Naturaleza".',
      },
      wherearewe: {
        title: 'La naturaleza es nuestro lugar',
        description: '<p>Ubicados entre los hermosos parajes de pinos en Cadalso de los Vidrios, Pinar del Concejo y Valle de las Tórtolas, con vistas a la Sierra de Gredos.</p><p>Aire puro con olor a pino y tierra mojada.</p>',
      },
      whatisit: {
        title: 'Medicinas ancestrales',
        description: '<p>Centro holistico de medicinas amazónicas y del mundo, que se han usado desde hace milenios para la cura de dolencias fisicas, psíquicas y espirituales.</p><p>Dietas amazónicas y chamánicas con plantas maestras, Ceremonias de Ayahuasca, Kambó, Yopo, Círculos de Rapé, y más.</p><p>Viajes a la selva peruana.</p>',
      },
      whoweare: {
        title: 'Más que medicina',
        description: 'Mi nombre es Carmen, facilitadora de medicinas ancestrales, me he formado en la selva peruana con chamanes y maestros vegetalistas de la tribu shipiba, casa de medicina San Alejandro. Mi primer contacto con las medicinas ancestrales fue...',
      },
      footer: {
        whatsapp: 'Consultame en WhatsApp',
        twitter: 'Seguime en Twitter',
        facebook: 'Siguime en Facebook',
        instagram: 'Siguime en Instagram',
        'privacy-policy': 'Politica de privacidad',
        'covid-policy': 'Medidas para el COVID-19',
        'site-map': 'Map web',
      },
    },
  },
};
