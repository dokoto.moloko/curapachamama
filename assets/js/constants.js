export const EMPTY_STRING = '';

export const DEVICES = {
  mobile: 'mobile',
  desktop: 'desktop',
};

export const ALLOWED_TAGS = ['strong', 'p', 'em'];
