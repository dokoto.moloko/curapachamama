const path = require('path');
const es = require('./locales/es');
const en = require('./locales/en');

export default {
  mode: 'universal',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    script: [
      {
        src: '/browserDetector.js'
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: { color: '#fff' },
  generate: {
    fallback: '400.html',
  },
  router: {
    extendRoutes (routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/ErrorPage.vue')
      })
    }
  },
  css: [
    '~assets/scss/main.scss',
  ],
  buildModules: [
    '@nuxtjs/eslint-module',
    '@nuxtjs/fontawesome',
  ],
  fontawesome: {
    icons: {
      brands: ['faInstagram', 'faFacebook', 'faTwitter', 'faWhatsapp'],
      solid: ['faBars', 'faPhone', 'faAt'],
    },
  },
  modules: [
    '@nuxtjs/style-resources',
    'nuxt-i18n',
    [
      '@nuxtjs/device',
      {
        defaultUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'
      },
    ],
  ],
  styleResources: {
    scss: [
      '~assets/scss/variables/**.scss',
      '~assets/scss/mixins/**.scss',
    ]
  },
  i18n: {
    locales: [
      {
        code: "en",
        name: "English"
      },
      {
        code: "es",
        name: "Español"
      },
    ],
    defaultLocale: 'es',
    vueI18n: {
      fallbackLocale: 'en',
      messages: { en, es },
    },
  },
  build: {
    extend (config) {
      config.resolve.alias['@'] = path.join(__dirname, '/');
      config.resolve.alias['@pages'] = path.join(__dirname, '/pages');
      config.resolve.alias['@layouts'] = path.join(__dirname, '/layouts');
      config.resolve.alias['@components'] = path.join(__dirname, '/components');
      config.resolve.alias['@sections'] = path.join(__dirname, '/sections');
      config.resolve.alias['@views'] = path.join(__dirname, '/views');
      config.module.rules.push({
        test: /\.(ogg|mp3|wav|mpe?g)$/i,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]'
        },
      });
    },
  },
  vue: {
    config: {
      resolve: {
        alias: {
          '@': path.join(__dirname, '/'),
          '@pages': path.join(__dirname, '/pages'),
          '@layouts': path.join(__dirname, '/layouts'),
          '@components': path.join(__dirname, '/components'),
          '@sections': path.join(__dirname, '/sections'),
          '@views': path.join(__dirname, '/views'),
        },
      },
    },
  },
  pageTransition: {
    name: 'page',
    mode: 'out-in',
  },
};
